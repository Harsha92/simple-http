provider "helm" {
  kubernetes {
    config_path = var.config_path
  }
}


resource "helm_release" "simple-http" {
  name       = "simple-http"
  chart      = "./simple-httpchart"
  namespace  = var.namespace
  values     = [file("./simple-httpchart/values.yaml")]
  create_namespace = true

  set {
    name = "image.tag"
    value = var.tag
  }

  set {
    name = "versionz.image.tag"
    value = var.tag
  }
}