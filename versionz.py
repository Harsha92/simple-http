from flask import Flask, request
import os
from prometheus_flask_exporter import PrometheusMetrics

app = Flask(__name__)
metrics = PrometheusMetrics(app)


@app.route("/")
def versionz():
    hash = os.environ.get('HASH')
    app_name = os.environ.get('APP_NAME')
    response = {
        "hash" : hash,
        "app_name" : app_name
    }
    return response


if __name__ == "__main__":
    app.run(host='0.0.0.0')
