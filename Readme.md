# Simple-Http
A Sample HTTP server that returns the app name and git hash of current commit id.
Repository also includes ways to deploy to a local Kubernetes cluster.

## Endpoint Details
This http server includes two endpoints :

- /helloworld
    This endpoint accepts query parameter of name. If no query parameter or one other than name is provided the endpoint returns the text `Hello Stranger`.
- /versionz
    This endpoint returns the name of the application and the githash of the current commit of the code.

## Usage Details

### Running it locally using make
Run and test the application with the command `make setup && make run_default`.
This starts the server by on port 8080 by default.

To run the app on a custom port export a shell variable using `export PORT=5000` and modify the above command to
`make setup && make run`.

### Running it locally using python3
The application can be started on a custom port with `python3 app.py [--port|-P]`. The default port is `8080`.

### Running it locally using docker
The docker file has been included to build the image. Build Image with `docker build -f Dockerfile . --build-arg GITHASH=value`
where `GITHASH` is the value of githash of current commit. It can be obtained by running `git rev-parse HEAD`

### Running it using Terraform
Deployment is done using the terraform helm provider.`TF_VAR_tag` needs to be exported before running terraform commands.

### Deploy to kubernetes
The application has been packaged as a chart and can be installed using the command `helm install release-name terraform/simple-httpchart`
Use minikube to test it out locally.
>The helm chart has a subchart for the versionz endpoint.

## Jenkins Docker File
A modified dockerfile for creating jenkins with the required binaries i.e kubectl and terraform has been added . Follow this [guide](https://www.jenkins.io/doc/book/installing/docker/) to run jenkins as a docker container. Use the modified docker in this repository instead of the one in the link.
>To deploy using jenkins to minikube locally they need to be on the same docker network so that jenkins can discover the minikube api server endpoint