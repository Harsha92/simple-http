from flask import Flask, request
import re
import os
import argparse
import sys
import requests
import json
from prometheus_flask_exporter import PrometheusMetrics


app = Flask(__name__)
metrics = PrometheusMetrics(app)

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    func()

def parse_args(args):
    parser = argparse.ArgumentParser(
        description='Start HTTP Server')

    parser.add_argument(
        '-P', '--port',
        help='port to start HTTP server on',
        type=int)
    return parser.parse_args(args)

@app.route("/helloworld")
def stranger():
    return ("Hello Stranger")

@app.route("/versionz")
def versionz():
    versionz_endpoint = os.getenv('VERSIONZ_EP')
    if versionz_endpoint == None:
        hash = os.environ.get('HASH')
        app_name = os.environ.get('APP_NAME')
        response = {
            "hash" : hash,
            "app_name" : app_name
        }
        return response
    else:
        if "http://" not in versionz_endpoint:
            versionz_endpoint = "http://" + versionz_endpoint
        versionz_response = requests.get(versionz_endpoint)
        response = json.loads(versionz_response.content.decode('utf-8'))
        response['resp_time'] = str(versionz_response.elapsed.total_seconds()) + 's'
        return response



@app.route('/shutdown')
def shutdown():
    shutdown_server()
    return 'Server shutting down...'


if __name__ == "__main__":
    color = os.getenv('COLOR')
    print(color)
    opts = parse_args(sys.argv[1:])
    port = opts.port
    if port == None:
        port = 8080
    app.run(host='0.0.0.0',port=port)
